var ng_js_utils = {
    removeItem: function (arr, item) {
        for (var i = arr.length; i--;) {
            if (arr[i] === item) {
                arr.splice(i, 1);
                break;
            }
        }
    },
    arrayUnion: function (arr1, arr2, equalityFunc) {
        var union = arr1.concat(arr2);
        for (var i = 0; i < union.length; i++) {
            for (var j = i + 1; j < union.length; j++) {
                if (equalityFunc(union[i], union[j])) {
                    union.splice(j, 1);
                    j--;
                }
            }
        }

        return union;
    },

    /**
     * Number.prototype.format(n, x, s, c)
     *
     * @param integer n: length of decimal
     * @param integer x: length of whole part
     * @param mixed   s: sections delimiter
     * @param mixed   c: decimal delimiter
     */
    formateNum: function (value) {
        var temp = value.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,")
        return temp.substring(0, temp.length - 3)
    },


    humanFileSize: function (size) {
        var i = Math.floor(Math.log(size) / Math.log(1024));
        return ( size / Math.pow(1024, i) ).toFixed(2) * 1 + ' ' + ['B', 'kB', 'MB', 'GB', 'TB'][i];
    },

    byString: function (o, s) {
        s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
        s = s.replace(/^\./, '');           // strip a leading dot
        var a = s.split('.');
        for (var i = 0, n = a.length; i < n; ++i) {
            var k = a[i];
            if (k in o) {
                o = o[k];
            } else {
                return;
            }
        }
        return o;
    },
    // printTemplate: function (context, tpl) {
    //     for (p in context)
    //         eval("var " + p + "=context." + p);
    //     var result = tpl.replace(/{[^}]*}/g, function (key) {
    //         var t = key.substring(1, key.length - 1)
    //         try {
    //             var value = eval(t)
    //             //add custom formatter here
    //             if (key == "{contType}") {
    //                 var selected = todoStore.contTypeList.find(function (t) {
    //                     return t.value == value
    //                 });
    //                 value = (selected == null ? null : selected.label);
    //             }
    //             // if (value instanceof moment) {
    //             //     value = value.format("DD/MM/YYYY")
    //             // }
    //             //default behaviour
    //             return value == null ? key : value
    //         } catch (ex) {
    //             return key
    //         }
    //     });
    //     return result;
    // },
    buildAjaxRequest2: function (url, callBack) {
        return {
            type: 'POST',
            url: url,
            success: callBack,
            error: function (xhr, status, err) {
                alert("failed" + err.toString());
                console.error(url, status, err.toString());
            }
        }
    },
    buildAjaxRequest: function (url, data, callBack) {
        return {
            contentType: 'application/json',
            type: 'POST',
            url: url,
            data: JSON.stringify(data),
            success: callBack,
            error: function (xhr, status, err) {
                alert("failed" + err.toString());
                console.error(url, status, err.toString());
            }
        }
    },

    sortByString: function (arr, field, desc){
        this.sortBy(arr, field, "string", desc)
    },

    sortBy: function (arr, field, fieldType, desc) {
        //fieldType options:string
        if (arr == null || arr.length == 0) return;

        if (fieldType == "string") {
            arr.sort(function (a, b) {
                var x = a[field].toLowerCase();
                var y = b[field].toLowerCase();
                if (desc == "desc") {
                    return y < x ? -1 : y > x ? 1 : 0;
                } else {
                    return x < y ? -1 : x > y ? 1 : 0;
                }

            })
        } else {
            arr.sort(function (a, b) {
                if (desc == "desc") {
                    return b[field] - a[field];
                } else {
                    return a[field] - b[field];

                }
            })
        }

    }
}

export default ng_js_utils
