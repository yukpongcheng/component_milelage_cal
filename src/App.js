import React from 'react';
import SectorPanel from './SectorPanel';
import './reactize.css'

var App = React.createClass({
    getInitialState: function() {
        return {

        };
    },

    componentDidMount:function(){

    },
    render: function () {
        var contextPath = window.ngConfig ? window.ngConfig.contextPath:'http://localhost:3001/';
        return (
            <SectorPanel contextPath={contextPath}/>
            // <SectorPanel contextPath="../../"/>

        );
    }
});
window.App=App;
export default App;
